<?php
$manifest = array(
	'acceptable_sugar_versions' => array(
		'regex_matches' => array(
			'6\.*',
		),
	),
	'acceptable_sugar_flavors' => array(
		0 => 'OS',
		1 => 'PRO',
		2 => 'ENT',
		3 => 'CE',
	),
	'readme' => '',
	'name' => 'Import_cstm',
	'description' => 'Кастомный импорт',
	'author' => 'sugartalk.ru',
	'published_date' => '10/04/2014',
	'version' => '1.0',
	'type' => 'module',
	'is_uninstallable' => true,
);

$installdefs = array(
	'id' => 'Asterisk',
	'copy' => array(
		array(
			'from' => '<basepath>/modules/Import/DoImport.php',
			'to' => 'modules/Import/DoImport.php',
		),
		array(
			'from' => '<basepath>/modules/Import/GetModules.php',
			'to' => 'modules/Import/GetModules.php',
		),
		array(
			'from' => '<basepath>/modules/Import/New.php',
			'to' => 'modules/Import/New.php',
		),
		array(
			'from' => '<basepath>/custom/include/globalControlLinks.php',
			'to' => 'custom/include/globalControlLinks.php',
		),
	),
);