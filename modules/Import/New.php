<?php
header('Content-type: text/html; charset=utf-8');
if (isset($_REQUEST['success'])) {
	echo '<div class="success">Импорт успешно завершен. Обработано <b>' . $_REQUEST['success'] . '</b> записей.</div><style>.success {font: 110% sans-serif; color: black; padding:2px 5px 2px 5px;}</style>';
} else {
	global $sugar_config, $current_user, $moduleList;
	$upload_dir = $sugar_config['upload_dir'];
	$file_name = 'importer_' . time() . '.csv';
	///////////есть файл
	if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $upload_dir . $file_name)) {
	///////////файл не выбран, показываем приветствие
		echo 'Выберите файл для импорта ';
		echo '<form enctype="multipart/form-data" action="index.php?module=Import&action=New" method="POST">
			<!-- Поле MAX_FILE_SIZE должно быть указано до поля загрузки файла -->
			<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
			<!-- Название элемента input определяет имя в массиве $_FILES -->
			<input name="userfile" type="file" />
			<input type="submit" value="Отправить файл" /></form>';
	}else{
		///////////файл выбран, показываем таблицу
		$f = fopen($upload_dir . $file_name, "r");
		//список модулей
		//$str = '<select class="modules_select">';
		$str .= '<option value=""></option>';
		foreach ($moduleList as $module) 
			if ((isset($GLOBALS['app_list_strings']['moduleList'][$module]))&&($module != 'Home')&&($module != 'Calendar')&&($module != 'Emails')&&($module != 'ProspectLists')&&($module != 'Documents')&&($module != 'Project')&&($module != 'Bugs'))
				$str .= '<option value="' 
				. $module 
				. '">' 
				. $GLOBALS['app_list_strings']['moduleList'][$module] 
				. '</option>';
		$str .= '</select>';
		$blank_select = '<option value=""></option></select>';
		
		echo '<hr>Выберите модуль для импорта: <select class="modules_main" onchange="get_modules($(\'.modules_main\').val())">' 
		. $str 
		. '    <button onclick="do_import(\'' . $upload_dir . $file_name . '\')">Начать импорт</button><hr>';
		
		$first = true;
		//////формирование таблицы
		echo "<html><body><table class='table_csv'>\n\n";
		while (($line = fgetcsv($f)) !== false) { // $line - строка
				echo "<tr>";
				$i = 0;
				foreach ($line as $cell) { // $cell - слово в строке
					if ($first) {
						echo "<td id='csv_head' class='td_csv_head" . $i . "'>" 
						. htmlspecialchars($cell) 
						. '</br><select class="td_csv_modules' . $i . '" onchange="get_submodules($(\'.td_csv_modules' . $i . '\').val(), ' . $i . ')">' 
						. $blank_select // </select>
						. '</br><select class="td_csv_fields' . $i . '"><option value=""></option>'
						. "</td>";
					} else {
						echo "<td class='td_csv'>" . htmlspecialchars($cell) . "</td>";
					};
					$i++;
				}
				$first = false;
				echo "</tr>\n";
		}
		echo "\n</table></body></html><style>.table_csv, .td_csv {font: 110% sans-serif; border: 1px solid black; border-collapse: collapse;padding:2px 5px 2px 5px;} table.table_csv td#csv_head {font: bold 110% sans-serif; border: 1px solid black; border-collapse: collapse;padding:2px 5px 2px 5px;}</style>";
		//////конец таблицы
		fclose($f);
		////////закрытие файла
		echo 'File: ' . $upload_dir . $file_name;
		//unlink ($upload_dir . $file_name);
	}
}
?>
<script>
	function get_submodules(module, n){
		if (module != ''){
			$.ajax({
				url: 'index.php?module=Import&action=GetModules&mode=sub&modules='+module+'&to_pdf=true',
				dataType: 'json',
				type: 'post',
				success: function(data)
				{
					$('.td_csv_fields' + n).html(data);
				},
				error: function(){alert('Ошибка приема данных ajax в get_submodules');}
			});
		} else $('.td_csv_fields' + n).html('<option value=""></option>');
	};
	
	function get_modules(module){
		if (module != ''){
			$.ajax({
				url: 'index.php?module=Import&action=GetModules&mode=rel&modules='+module+'&to_pdf=true',
				dataType: 'json',
				type: 'post',
				success: function(data)
				{	
					var tmp = '';
					tmp = '<option value=""></option><option value="' + $('.modules_main').val() + '">' + $('.modules_main option:selected').text() + '</option>';
					$('[class^=td_csv_modules]').html(tmp + data);
				},
				error: function(){alert('Ошибка приема данных ajax в get_modules');}
			});
		} else {
			$('[class^=td_csv_modules]').html('<option value=""></option>');
			$('[class^=td_csv_fields]').html('<option value=""></option>');
		};
	};
	
	function do_import(file){
		if ($('.modules_main').val().length == 0){alert('Модуль и поля для импорта не выбраны');}else{
			// find map
			var map = '';
			console.log('=====================================map=================================');
			$('[class^=td_csv_modules]').each(function( index ) {
				if (($( this ).val() != '') && ($('.td_csv_fields'+index).val() != '')) {
					console.log( index + ":" + $( this ).val() + ":" + $('.td_csv_fields'+index).val() );
					map += index + ":" + $( this ).val() + ":" + $('.td_csv_fields'+index).val() + ' ';
				};
			});
			console.log( 'map = ' + map );
			// compile data array
			var data = [map, 0, ];
			var module = $('.modules_main').val();
			// send map, module, file_name
			$.ajax({
				url: 'index.php?module=Import&action=DoImport&to_pdf=true',
				dataType: 'json',
				data: {
						'map': map,
						'file': file,
						'modu': module ///////////////////////////// 'module' ---> 'modu' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					},
				type: 'post',
				success: function(data)
				{	
					//alert(data);
					document.location.href = document.location.href + '&success=' + data;
				},
				error: function(){alert('Ошибка приема данных ajax');}
			});
		};
	};
</script>