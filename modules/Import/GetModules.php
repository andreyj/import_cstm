<?php
header('Content-type: text/html; charset=utf-8');
global $sugar_config, $moduleList, $dictionary;

if (isset($_REQUEST['modules'])){
	if (isset($_REQUEST['mode'])){
		if ($_REQUEST['mode'] == 'sub'){
			$module_name = $_REQUEST['modules'];
			
			require_once('modules/ModuleBuilder/views/view.modulefields.php');
			$viewmodfields = new ViewModulefields();
			$objectName = BeanFactory::getObjectName($module_name);
			VardefManager::loadVardef($module_name, $objectName, true);
			global $dictionary;
			
			$str = '<option value=""></option>';
			
			$fieldsData = array();
			foreach($dictionary[$objectName]['fields'] as $def) {
				if ($viewmodfields->isValidStudioField($def))
				{
					$str .= '<option value="' . $def['name'] . '">' . translate($def['vname'], $module_name) . '</option>';
				}
			};
			echo json_encode($str);
		}
		
		if ($_REQUEST['mode'] == 'rel'){
			//$str = '<select class="module_rel' . $_REQUEST['n'] . '">';
			$str = '';
			$module_name = $_REQUEST['modules'];
			$module = BeanFactory::newBean($module_name);
			$linked_fields = $module->get_linked_fields();
			foreach($linked_fields as $name=>$properties)
			{
				if ((isset($properties['module'])) && (isset($GLOBALS['app_list_strings']['moduleList'][$properties['module']])) && (strpos($str, $properties['module']) == false)) {
					$str .= '<option value="' . $properties['module'] . '">' . $GLOBALS['app_list_strings']['moduleList'][$properties['module']] . '</option></br>';
				};
			}
			//$str .= '</select>';
			echo json_encode($str);
		}
	}
}

/*$acc = new Account();
	$linked_fields=$acc->get_linked_fields();
        //foreach($linked_fields as $name=>$properties)
        foreach($linked_fields as $name=>$properties)
        {
            echo $properties['module'] . ' ';
			//$acc->load_relationship($name);
        }*/

?>